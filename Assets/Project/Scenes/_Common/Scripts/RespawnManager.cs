﻿namespace Assets.Project.Scenes._Common.Scripts
{
    using System.Collections;
    using Assets.Project.Player.Scripts.Respawn;
    using Mirror;
    using UnityEngine;

    internal class RespawnManager : NetworkBehaviour
    {
        private readonly SyncList<PlayerRespawn> Players = new SyncList<PlayerRespawn>();
        private static RespawnManager _instance;

        [Header("Respawn Settings")]
        [SerializeField]
        private float _respawnDelayTime = 5f;

        private bool _isInRespawnMode;

        public static RespawnManager Instance => _instance;

        [Server]
        public void AddPlayer(PlayerRespawn player)
        {
            Players.Add(player);
        }

        [Server]
        public void RemovePlayer(PlayerRespawn player)
        {
            Players.Remove(player);
        }

        [Server]
        public void RespawnAll()
        {
            if (!_isInRespawnMode)
            {
                StartCoroutine(DelayedRespawnAll());
            }
        }

        [Server]
        private IEnumerator DelayedRespawnAll()
        {
            _isInRespawnMode = true;

            yield return new WaitForSeconds(_respawnDelayTime);

            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Respawn();
            }

            _isInRespawnMode = false;
        }

        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
            }
            else if (_instance == this)
            {
                Destroy(gameObject);
            }
        }
    }
}
