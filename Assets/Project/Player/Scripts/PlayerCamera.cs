﻿namespace Assets.Project.Player.Scripts
{
    using Mirror;
    using UnityEngine;

    internal class PlayerCamera : NetworkBehaviour
    {
        [Header("Camera Holder")]
        [SerializeField]
        private Transform _cameraHolderTransform;

        [SerializeField]
        private Vector3 _cameraLocalPosition = new Vector3(0.5f, 0.5f, -4.5f);

        [Header("Camera Look")]
        [SerializeField]
        private float _lookXMin = -20f;

        [SerializeField]
        private float _lookXMax = 60f;

        [SerializeField]
        private float _lookSpeed = 2f;

        [SerializeField]
        private bool _isVerticallyInverted;

        [Header("Camera Collision Detection")]
        [SerializeField]
        private float _collisionDetectionRadius = 0.15f;

        [SerializeField]
        [Tooltip("How fast the camera should snap into the default position if there are no obstacles")]
        private float _snapSpeed = 15f;

        private Transform _cameraTransform;
        private Vector2 _playerRotation;
        private Vector3 _defaultCameraPosition;
        private float _defaultCameraPositionDistance;

        public Transform CameraTransform => _cameraTransform;

        public override void OnStartLocalPlayer()
        {
            SetupCamera();
            SetupValues();
            SetupCursor();
        }

        private void Update()
        {
            if (!isLocalPlayer)
            {
                return;
            }

            Vector2 mouseAxis = PlayerInput.MouseAxis * _lookSpeed;

            if (_isVerticallyInverted)
            {
                mouseAxis.y *= -1f;
            }

            _playerRotation.y += mouseAxis.x;
            _playerRotation.x += mouseAxis.y;
            _playerRotation.x = Mathf.Clamp(_playerRotation.x, _lookXMin, _lookXMax);

            _cameraHolderTransform.localRotation = Quaternion.Euler(_playerRotation.x, 0f, 0f);
            transform.eulerAngles = new Vector3(0f, _playerRotation.y, 0f);
        }

        private void LateUpdate()
        {
            if (!isLocalPlayer)
            {
                return;
            }

            Vector3 directionToCamera = _cameraTransform.position - _cameraHolderTransform.position;

            _cameraTransform.localPosition = Physics.SphereCast(_cameraHolderTransform.position, _collisionDetectionRadius, directionToCamera, out RaycastHit hit, _defaultCameraPositionDistance)
                ? Vector3.Normalize(_defaultCameraPosition) * (hit.distance - _collisionDetectionRadius)
                : Vector3.Lerp(_cameraTransform.localPosition, _defaultCameraPosition, _snapSpeed * Time.deltaTime);
        }

        private void SetupCamera()
        {
            Camera camera = Camera.main;

            if (camera == null)
            {
                Debug.LogError("Main camera is not found!");
                return;
            }

            _cameraTransform = camera.transform;
            _cameraTransform.SetParent(_cameraHolderTransform);
            _cameraTransform.localPosition = _cameraLocalPosition;
        }

        private void SetupValues()
        {
            _defaultCameraPosition = _cameraTransform.localPosition;
            _defaultCameraPositionDistance = Vector3.Distance(_cameraTransform.position, _cameraHolderTransform.position);
            _playerRotation.y = _cameraTransform.eulerAngles.y;
        }

        private void SetupCursor()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
}
