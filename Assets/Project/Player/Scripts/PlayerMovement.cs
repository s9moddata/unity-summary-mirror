﻿namespace Assets.Project.Player.Scripts
{
    using Assets.Project.Player.Scripts.Respawn;
    using Mirror;
    using UnityEngine;

    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(SurfaceChecker))]
    [RequireComponent(typeof(PlayerCamera))]
    internal class PlayerMovement : NetworkBehaviour, IRespawn
    {
        [Header("Speed")]
        [SerializeField]
        private float _walkSpeed = 40f;

        [SerializeField]
        private float _dashSpeed = 300f;

        [SerializeField]
        private float _dashDistance = 10f;

        [SerializeField]
        [Tooltip("Multiply speed when player is not on surface")]
        [Range(0f, 1f)]
        private float _airSpeedMultiplier = 0.4f;

        [Header("Drag")]
        [SerializeField]
        private float _surfaceDrag = 6f;

        [SerializeField]
        private float _airDrag = 2.25f;

        [SyncVar]
        private bool _isDashing;

        [SyncVar]
        private float _dashingPassedDistance;

        [SyncVar]
        private Vector3 _previousDashingPosition;

        private Rigidbody _rigidbody;
        private SurfaceChecker _surfaceChecker;
        private PlayerCamera _playerCamera;

        public bool IsDashing
        {
            get => _isDashing;
            [Server]
            private set
            {
                if (_isDashing == value)
                {
                    return;
                }

                if (!value)
                {
                    _dashingPassedDistance = 0f;
                }

                _rigidbody.useGravity = !value;
                _isDashing = value;
            }
        }

        private Vector3 MoveDirectionNormalized
        {
            get
            {
                Vector2 axisInput = PlayerInput.AxisNormalized;
                Vector3 moveDirection = _playerCamera.CameraTransform.right * axisInput.x + _playerCamera.CameraTransform.forward * axisInput.y;
                moveDirection.y = 0f;

                return _surfaceChecker.IsOnSlope
                    ? Vector3.ProjectOnPlane(moveDirection, _surfaceChecker.SurfaceNormal).normalized
                    : moveDirection.normalized;
            }
        }

        [Server]
        public void Respawn()
        {
            IsDashing = false;
        }

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _surfaceChecker = GetComponent<SurfaceChecker>();
            _playerCamera = GetComponent<PlayerCamera>();
        }

        private void Update()
        {
            if (isLocalPlayer)
            {
                CheckDashingInput();
                _rigidbody.drag = _surfaceChecker.IsOnGentleSurface ? _surfaceDrag : _airDrag;
            }

            if (isServer)
            {
                CheckDashing();
            }
        }

        private void FixedUpdate()
        {
            if (isLocalPlayer)
            {
                MoveOnSlope();
                Move();
            }
        }

        [Client]
        private void CheckDashingInput()
        {
            if (PlayerInput.Dash && !IsDashing)
            {
                CmdDash();
            }
        }

        [Command]
        private void CmdDash()
        {
            IsDashing = true;
        }

        [Server]
        private void CheckDashing()
        {
            if (IsDashing)
            {
                float passedDistance = Vector3.Distance(transform.position, _previousDashingPosition);
                _dashingPassedDistance += passedDistance;

                if (_dashingPassedDistance >= _dashDistance || passedDistance == 0f)
                {
                    IsDashing = false;
                }
            }

            _previousDashingPosition = transform.position;
        }

        private void MoveOnSlope()
        {
            if (_surfaceChecker.IsOnSlope)
            {
                Vector3 slipDirection = _surfaceChecker.SurfaceNormal + Physics.gravity.normalized;
                Vector3 antiSlipVelocity = slipDirection * Physics.gravity.magnitude * -1f;
                Move(antiSlipVelocity);
            }
        }

        private void Move()
        {
            float speed;

            if (IsDashing)
            {
                speed = _dashSpeed;
            }
            else
            {
                speed = _walkSpeed;

                if (!_surfaceChecker.IsOnGentleSurface)
                {
                    speed *= _airSpeedMultiplier;
                }
            }

            Move(MoveDirectionNormalized * speed);
        }

        private void Move(Vector3 force) => _rigidbody.AddForce(force, ForceMode.Acceleration);
    }
}
