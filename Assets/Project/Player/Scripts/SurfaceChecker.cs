﻿namespace Assets.Project.Player.Scripts
{
    using UnityEngine;

    [RequireComponent(typeof(CapsuleCollider))]
    internal class SurfaceChecker : MonoBehaviour
    {
        private readonly Vector3 FlatGroundNormal = Vector3.up;

        [Header("Ground Detection")]
        [SerializeField]
        private float _surfaceCheckDistance = 0.2f;

        [SerializeField]
        private float _colliderRadiusReduction = 0.1f;

        [SerializeField]
        private LayerMask _surfaceMask;

        [Header("Slope Detection")]
        [Range(0f, 1f)]
        [SerializeField]
        private float _slopeAngleMax = 0.385f;

        private CapsuleCollider _capsuleCollider;
        private bool _isOnGentleSurface;
        private Vector3 _surfaceNormal;

        public bool IsOnGentleSurface => _isOnGentleSurface;

        public bool IsOnSlope => _surfaceNormal.y != FlatGroundNormal.y;

        public Vector3 SurfaceNormal => _surfaceNormal;

        private void Start()
        {
            _capsuleCollider = GetComponent<CapsuleCollider>();
        }

        private void Update()
        {
            CheckSurface();
        }

        private void CheckSurface()
        {
            _isOnGentleSurface = GetSurfaceCast(out RaycastHit raycastHit) && raycastHit.normal.y > 1f - _slopeAngleMax;
            _surfaceNormal = _isOnGentleSurface ? raycastHit.normal : FlatGroundNormal;
        }

        private bool GetSurfaceCast(out RaycastHit raycastHit)
        {
            float distance = _capsuleCollider.height * 0.5f + _surfaceCheckDistance;
            float radius = _capsuleCollider.radius - _colliderRadiusReduction;
            return Physics.SphereCast(transform.position, radius, Vector3.down, out raycastHit, distance, _surfaceMask);
        }
    }
}
