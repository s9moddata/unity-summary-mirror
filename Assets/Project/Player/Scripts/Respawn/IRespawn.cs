﻿namespace Assets.Project.Player.Scripts.Respawn
{
    internal interface IRespawn
    {
        void Respawn();
    }
}
