﻿namespace Assets.Project.Player.Scripts.Respawn
{
    using Assets.Project.Scenes._Common.Scripts;
    using Mirror;
    using UnityEngine;

    internal class PlayerRespawn : NetworkBehaviour
    {
        private IRespawn[] _respawnAffectedComponents;

        public override void OnStartServer()
        {
            _respawnAffectedComponents = GetComponents<IRespawn>();
            RespawnManager.Instance.AddPlayer(this);
        }

        public override void OnStopServer()
        {
            RespawnManager.Instance.RemovePlayer(this);
        }

        [Server]
        public void Respawn()
        {
            for (int i = 0; i < _respawnAffectedComponents.Length; i++)
            {
                _respawnAffectedComponents[i].Respawn();
            }

            RpcSetStartPosition();
        }

        [ClientRpc]
        private void RpcSetStartPosition()
        {
            int startPositionIndex = Random.Range(0, NetworkManager.startPositions.Count);
            transform.position = NetworkManager.startPositions[startPositionIndex].position;
        }
    }
}
