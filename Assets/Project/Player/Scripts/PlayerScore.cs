﻿namespace Assets.Project.Player.Scripts
{
    using Assets.Project.Player.Scripts.Respawn;
    using Assets.Project.Scenes._Common.Scripts;
    using Mirror;
    using UnityEngine;

    internal class PlayerScore : NetworkBehaviour, IRespawn
    {
        [Header("Score Info")]
        [SerializeField]
        private TextMesh _scoreText;

        [SerializeField]
        private Transform _scoreTextHolderTransform;

        [SerializeField]
        private int _scoreToWin = 1;

        [SyncVar(hook = nameof(OnScoreChanged))]
        private int _score;

        private Camera _mainCamera;

        public override void OnStartLocalPlayer()
        {
            Vector3 scoreHolderLocalScale = _scoreTextHolderTransform.localScale;
            scoreHolderLocalScale.x *= -1f;
            _scoreTextHolderTransform.localScale = scoreHolderLocalScale;
        }

        [Server]
        public void Respawn()
        {
            _score = 0;
        }

        [Server]
        public void IncreaseScore()
        {
            _score++;

            if (_score == _scoreToWin)
            {
                RespawnManager.Instance.RespawnAll();
            }
        }

        private void OnScoreChanged(int oldScore, int newScore)
        {
            _scoreText.text = newScore.ToString();
        }

        private void Start()
        {
            if (!isLocalPlayer)
            {
                _mainCamera = Camera.main;
            }
        }

        private void Update()
        {
            if (!isLocalPlayer && _mainCamera != null)
            {
                _scoreTextHolderTransform.LookAt(_mainCamera.transform);
            }
        }
    }
}
