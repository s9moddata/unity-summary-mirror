﻿namespace Assets.Project.Player.Scripts
{
    using System.Collections;
    using Assets.Project.Player.Scripts.Respawn;
    using Mirror;
    using UnityEngine;

    [RequireComponent(typeof(PlayerMovement))]
    [RequireComponent(typeof(PlayerScore))]
    internal class PlayerDasher : NetworkBehaviour, IRespawn
    {
        [Header("Invincibility Settings")]
        [SerializeField]
        private Color _defaultColor = Color.white;

        [SerializeField]
        private Color _invincibilityColor = Color.red;

        [SerializeField]
        private float _invincibilityDuration = 3f;

        [Header("Mesh")]
        [SerializeField]
        private MeshRenderer _meshRenderer;

        [SyncVar(hook = nameof(OnColorChanged))]
        private Color _color;

        [SyncVar]
        private bool _isInvincible;

        private PlayerMovement _playerMovement;
        private PlayerScore _playerScore;
        private Material _meshMaterial;

        public bool IsInvincible => _isInvincible;

        public override void OnStartLocalPlayer()
        {
            _playerMovement = GetComponent<PlayerMovement>();
        }

        [Server]
        public void Respawn()
        {
            _isInvincible = false;
            _color = _defaultColor;
        }

        [Server]
        public void Hit()
        {
            if (!_isInvincible)
            {
                StartCoroutine(MakeInvincible());
            }
        }

        [Server]
        private IEnumerator MakeInvincible()
        {
            _isInvincible = true;
            _color = _invincibilityColor;

            double invincibilityExpirationTime = NetworkTime.time + _invincibilityDuration;

            while (invincibilityExpirationTime > NetworkTime.time)
            {
                yield return null;
            }

            _isInvincible = false;
            _color = _defaultColor;
        }

        private void OnColorChanged(Color oldColor, Color newColor)
        {
            if (_meshMaterial == null)
            {
                _meshMaterial = new Material(_meshRenderer.material);
            }

            _meshMaterial.color = newColor;
            _meshRenderer.material = _meshMaterial;
        }

        private void Start()
        {
            _playerScore = GetComponent<PlayerScore>();
        }

        private void OnDestroy()
        {
            Destroy(_meshMaterial);
        }

        private void OnCollisionEnter(Collision collision)
        {
            bool canDash = isLocalPlayer && _playerMovement.IsDashing;

            if (canDash && collision.gameObject.TryGetComponent(out PlayerDasher hitPlayerDasher) && !hitPlayerDasher.IsInvincible)
            {
                CmdHit(hitPlayerDasher);
            }
        }

        [Command]
        private void CmdHit(PlayerDasher hitPlayerDasher)
        {
            hitPlayerDasher.Hit();
            _playerScore.IncreaseScore();
        }
    }
}
